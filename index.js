// const { prisma } = require("./generated/prisma-client");

// // A `main` function so that we can use async/await
// async function main() {
//   // Create a new user with a new post
//   //   const newUser = await prisma.createUser({
//   //     name: "Bob",
//   //     email: "bob@prisma.io",
//   //     post: {
//   //       create: {
//   //         title: "The data layer for modern apps"
//   //       }
//   //     }
//   //   });
//   //   console.log(`Created new user: ${newUser.name} (ID: ${newUser.id})`);

//   //   // Read all users from the database and print them to the console
//   //   const allUsers = await prisma.users();
//   //   console.log(allUsers);

//   //   const allPosts = await prisma.posts();
//   //   console.log(allPosts);

//   const postsByUser = await prisma.user({ email: "bob@prisma.io" }).posts();
//   console.log(`All posts by that user: ${JSON.stringify(postsByUser)}`);
// }

// main().catch(e => console.error(e));

const { prisma } = require('./generated/prisma-client');
const { GraphQLServer } = require('graphql-yoga');

const resolvers = {
	Query: {
		todos: (parent, args, context) => {
			return context.prisma.todoes();
		}
	},
	Mutation: {
		createTodo(parent, args, context) {
			return context.prisma.createTodo({
				todo: args.todo,
				isSelect: args.isSelect
			});
		},
		updateTodo(parent, args, context) {
			let { todo, id } = args;
			return context.prisma.updateTodo({
				data: {
					todo: todo
				},
				where: {
					id
				}
			});
		},
		deleteTodo: async (parent, args, context) => {
			let { id } = args;
			try {
				await context.prisma.deleteTodo({
					id
				});
			} catch (e) {
				console.log('error todo');
			}
			return 'Successful delete';
		}
	}
};

const server = new GraphQLServer({
	typeDefs: './schema.graphql',
	resolvers,
	context: {
		prisma
	}
});
server.start(() => console.log('Server is running on http://localhost:4000'));
